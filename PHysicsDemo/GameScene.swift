//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    
    override func didMove(to view: SKView) {
        circle.position = CGPoint(x:self.size.width*0.25, y:self.size.height/2)
        square.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        triangle.position = CGPoint(x:self.size.width*0.75, y:self.size.height/2)
        
        addChild(circle)
        addChild(square)
        addChild(triangle)
        
        // make boundary
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        // 1. Create the hitboxes
        
        self.circle.physicsBody = SKPhysicsBody(circleOfRadius: self.circle.frame.width/2)
//        let trianglePath = CGMutablePath()
//        trianglePath.move(to: CGPoint(x: -triangle.size.width/2,
//
//                                      y: -triangle.size.height/2))
//        trianglePath.addLine(to: CGPoint(x: triangle.size.width/2,
//
//                                         y: -triangle.size.height/2))
//        trianglePath.addLine(to: CGPoint(x: 0, y: triangle.size.height/2))
//        trianglePath.addLine(to: CGPoint(x: -triangle.size.width/2,
//
//                                         y: -triangle.size.height/2))
//        triangle.physicsBody = SKPhysicsBody(polygonFrom: trianglePath)
        
        // 2. turn off gravity
        self.circle.physicsBody!.affectedByGravity = true
        self.square.physicsBody = SKPhysicsBody(rectangleOf: self.square.frame.size)
        self.triangle.physicsBody = SKPhysicsBody(texture: triangle.texture!, size: triangle.size)
        self.square.physicsBody!.affectedByGravity = true
        self.triangle.physicsBody!.affectedByGravity = false
        // Bouncing
        self.circle.physicsBody!.restitution = 1.0
        //self.triangle.physicsBody!.restitution = 1.0
        //self.triangle.physicsBody!.angularVelocity = 1.0
        self.circle.physicsBody?.density = 30
        self.square.physicsBody!.restitution = 1.0

        spawnSand()
    }
    
    
    func spawnSand() {
        let sand = SKSpriteNode(imageNamed: "sand")
        let x = self.size.width/2
        let y = self.size.height - 100
        sand.position.x = x
        sand.position.y = y
        sand.physicsBody = SKPhysicsBody(rectangleOf: sand.frame.size)
        sand.physicsBody!.affectedByGravity = true
        sand.physicsBody!.restitution = 1.0
        sand.physicsBody!.density = 40
        addChild(sand)
    }
    
    override func update(_ currentTime: TimeInterval) {
        self.spawnSand()
    }
    
}
